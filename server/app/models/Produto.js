var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

// Define o esquema do Produto
var produtoSchema = new Schema({
	id			: Schema.ObjectId,
    nome        : String,
    descricao   : String,
    estoque     : Number,
    preco     : Number,
    
    //relacoes
    categoria: Number, //FamiliaId
    
    //enums
    situacaoProduto: Number,
    tipoProduto: Number,
    unidadeMedida: Number,
    
    dataCriacao : { type: Date, default: Date.now },
    ultimaAtualizacao: {type: Date}
});



module.exports = mongoose.model('produto', produtoSchema);
