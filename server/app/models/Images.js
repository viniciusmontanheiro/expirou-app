/**
 * Created by vcrzy on 11/11/15.
 */

'use strict';

var mongoose = require('mongoose');
var imageSchema = mongoose.Schema({
    produto_id: {type: String, required: true},
    slideshow: {type: Boolean},
    principal: {type: Boolean},
    image: {
        modificationDate: {type: Date},
        name: {type: String},
        size: {type: Number},
        type: {type: String},
        filename: {type: String}
    },
    modificationDate: {type: Date, "default": Date.now}
});

module.exports = mongoose.model("image", imageSchema);
