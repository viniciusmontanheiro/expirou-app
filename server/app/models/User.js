
/**
 * Created by vinicius on 10/04/15.
 */


var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// Define o esquema do Usuário

var userSchema = new mongoose.Schema({
    ativo       : Boolean,
    nome        : String,
    email       : { type : String, unique: true},
    password    : { type: String, required: true, trim:true },
    telefone    : String,
    dataCriacao        : { type: Date, default: Date.now },
    ultimaAtualizacao: {type: Date},
    cidade      :String,
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.set('toJSON', {
    transform: function(doc, ret, options) {
        delete ret.password;
        return ret;
    }
});

module.exports = mongoose.model('user', userSchema);
