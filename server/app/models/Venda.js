var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// Define o esquema do Produto
var vendaSchema = mongoose.Schema({
    id			: Number,
    quantidade 	: Number,
    
    //Relacoes
    produto		: Number, //ProdutoId
    venda		: Number, //VendaId
    usuario		: Number, //UserId
    
    //Enums
    situacaoVenda : Number
    
});



module.exports = mongoose.model('venda', vendaSchema);
