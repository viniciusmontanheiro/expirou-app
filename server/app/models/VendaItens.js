var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// Define o esquema do Produto
var vendaSchema = mongoose.Schema({
    id			: Number,
    data : { type: Date, default: Date.now },
    observacao     : String,
    
    //Enums
    situacaoVenda: Number
    
});



module.exports = mongoose.model('vendaItens', vendaSchema);
