var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// Define o esquema do CategoriaProduto
var categoriaProdutoSchema = new mongoose.Schema({
    id			: Number,
    descricao    : String
});

module.exports = mongoose.model('categoriaProduto', categoriaProdutoSchema);
