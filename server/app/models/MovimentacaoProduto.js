var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// Define o esquema do Produto
var movimentacaoProdutoSchema = mongoose.Schema({

    dataMovimentacao : { type: Date, default: Date.now },
    quantidade     : Number,
    observacao     : String,
    
    //relacoes
    produto: Number, //ProdutoId
    
    //enums
    tipoMovimentacao: Number
});

module.exports = mongoose.model('movimentacaoProduto', movimentacaoProdutoSchema);
