'use strict';

/**
 * Created by viniciusmontanheiro on 13/07/15.
 */

function AccountController() {

    var controller = {

        login : function login(passport){
            return passport.authenticate('admin-login', {
                successRedirect : '/manager',
                failureRedirect : '/',
                failureFlash : true
            });
        },

        logout : function logout(req, res, next){
            req.logout();
            return res.redirect('/');
        }
    };
    return controller;
};
module.exports = new AccountController();