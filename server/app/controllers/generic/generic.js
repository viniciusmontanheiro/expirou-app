/**
 *@description Implementação do generic controller
 * @param req
 * @param res
 * @param next
 */

function GenericController() {

    var controller = {
        error: {
            notFound: function (req, res, next) {
                res.render('error/404', {
                    lang: 'pt-br'
                });
                next();
            },
            secure: function (req, res, next) {
                if (req.protocol != "https") {
                    res.set('x-forwarded-proto', 'https');
                    res.redirect('https://' + req.get('host') + req.url);
                } else {
                    next();
                }
            },
            badUser: function(req,res,next){
                var m = new Messages();

                m.LOGINERROR.DATA = {
                    title : 'Arquitetura Admin'
                };
                res.render('index',m.LOGINERROR);
            }
        }
    };
    return controller;
};
module.exports = new GenericController();
