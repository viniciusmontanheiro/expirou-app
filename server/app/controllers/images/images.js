
'use strict';

/**
 * @desc Controle responsável pelo tratamento das imagens
 * @since 02/12/2014
 */

var mongoose = require('mongoose')
    , fs = require('fs')
    , Image = require('../../models/Images');

// NEW UPLOAD
exports.upload = function (req, res) {
    var oldPath = req.files.myFile.path;
    var separator = '/';
    var filename = oldPath.split(separator)[oldPath.split(separator).length - 1];
    var newPath = __dirname;
    newPath = newPath.split("/server/");
    newPath = [newPath[0],'client', 'assets', 'images', 'uploads', filename].join(separator);
    //MUDAR O CAMINHO PARA S.O WINDOWS


    console.log('>>>>>');
    console.log('__dirname', __dirname);
    console.log('oldPath', oldPath);
    console.log('newPath: ', newPath);
    console.log('filename: ', filename);
    console.log('Product: ', req.body);

    fs.rename(oldPath, newPath, function (err) {
        if (err === null) {
            var image = {
                produto_id: req.body.produto_id || 999,
                slideshow: req.body.slideshow || false,
                principal: req.body.principal || false,
                image: {
                    modificationDate: req.files.myFile.modifiedDate || new Date(),
                    name: req.files.myFile.name || "???",
                    size: req.files.myFile.size || 0,
                    type: req.files.myFile.type || "???",
                    filename: filename
                }
            };
            var doc = new Image(image);

            console.log('Renaming file to ', req.files.myFile.name);

            doc.save(function (err) {

                var retObj = {
                    meta: {"action": "upload", 'timestamp': new Date(), filename: __filename},
                    doc: doc,
                    err: err
                };
                return res.send(retObj);
            });
        }
    });
};

// CREATE
exports.create = function (req, res) {
    var doc = new Image(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};

// GET BY PRODUCT ID
//'use strict';
//
///**
// * @desc Controle responsável pelo tratamento das imagens
// * @since 02/12/2014
// */
//
//var mongoose = require('mongoose')
//    , fs = require('fs')
//    , Image = require('../models/images')
//    , Validate = global.validates
//    , Util = global.utils
//    , events = require('events')
//    , Event = new events.EventEmitter();
//
//
//// NEW UPLOAD
//exports.upload = function (req, res) {
//    var oldPath = req.files.myFile.path;
//    var separator = '/';
//    var filename = oldPath.split(separator)[oldPath.split(separator).length - 1];
//    var newPath = [__dirname, '..', '..', '..', 'client', 'assets', 'images', 'uploads', '', filename].join(separator);
//    //var newPath ="C:\\Users\\Vini\\AppData\\Local\\Temp\\";
//
//    console.log('>>>>>');
//    console.log('__dirname', __dirname);
//    console.log('oldPath', oldPath);
//    console.log('newPath: ', newPath);
//    console.log('filename: ', filename);
//    console.log('Product: ', req.body);
//
//    fs.rename(oldPath, newPath, function (err) {
//        if (err === null) {
//            var image = {
//                produto_id: req.body.produto_id || "???",
//                slideshow: req.body.slideshow || "???",
//                principal: req.body.principal || "???",
//                image: {
//                    modificationDate: req.files.myFile.modifiedDate || new Date(),
//                    name: req.files.myFile.name || "???",
//                    size: req.files.myFile.size || 0,
//                    type: req.files.myFile.type || "???",
//                    filename: filename
//                }
//            };
//            var doc = new Image(image);
//
//            console.log('Renaming file to ', req.files.myFile.name);
//
//            doc.save(function (err) {
//
//                var retObj = {
//                    meta: {"action": "upload", 'timestamp': new Date(), filename: __filename},
//                    doc: doc,
//                    err: err
//                };
//                return res.send(retObj);
//            });
//        }
//    });
//};
//
//// CREATE
//exports.create = function (req, res) {
//    var doc = new Image(req.body);
//
//    doc.save(function (err) {
//        var retObj = {
//            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
//            doc: doc,
//            err: err
//        };
//        return res.send(retObj);
//    });
//}
//
//// GET BY PRODUCT ID

//exports.listaByProdutoId = function (req, res) {
//    var id = req.query.produto_id;
//
//    Image.find({'produto_id': id}, function (err, images) {
//        if (err) {
//            return Event.emit("LOGGER", Util.messageProcess(res, Util.message.HANDLERROR))
//        }
//        Util.message.GETTED.DATA = images;
//        return Event.emit("LOGGER", Util.messageProcess(res, Util.message.GETTED));
//    });
//};

// GET BY IMAGE AND PRODUCT ID

//
//// GET BY IMAGE AND PRODUCT ID

//exports.listaByImageIdProdutoId = function (req, res) {
//    var produtoId = req.query.produto_id;
//    var id = req.query_id;
//
//    Image.find({_id: id, 'produto_id': produtoId}, function (err, images) {
//        if (err) {
//            return Event.emit("LOGGER", Util.messageProcess(res, Util.message.HANDLERROR))
//        }
//        Util.message.GETTED.DATA = images;
//        return Event.emit("LOGGER", Util.messageProcess(res, Util.message.GETTED));
//    });
//};


// UPDATE

//
//// UPDATE

//exports.update = function (req, res) {
//    var imagem = req.body;
//    var id = imagem._id;
//    delete imagem._id;
//
//    if (!id || id == undefined) {
//        return Event.emit("LOGGER", Util.messageProcess(res, Util.message.CHANGERROR));
//    }
//    Image.update({_id: id}, imagem, {upsert: true}, function (err) {
//        if (err) {
//            return Event.emit("LOGGER", Util.messageProcess(res, Util.message.CHANGERROR));
//        }
//        return Event.emit("LOGGER", Util.messageProcess(res, Util.message.CHANGED));
//    });
//};


// DELETE

//
//// DELETE

//exports.delete = function (req, res) {
//    var id = req.query._id;
//
//    if (!id || id == undefined) {
//        return Event.emit("LOGGER", Util.messageProcess(res, Util.message.REMOVERROR));
//    }
//    Image.find({_id: id}).remove(function (err) {
//        if (err) {
//            return Event.emit("LOGGER", Util.messageProcess(res, Util.message.REMOVERROR));
//        }
//        return Event.emit("LOGGER", Util.messageProcess(res, Util.message.REMOVED));
//    });

//};

