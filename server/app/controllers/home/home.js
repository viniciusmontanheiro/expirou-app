'use strict';

/**
 * @description Implementação do index controller
 */

function HomeController() {

    var controller = {

        home: function init(req, res, next) {
            var m = new Messages();
            m.GETTED.DATA = {
                titulo: 'Expirou ???'
            };
            m.GETTED.MESSAGE = req.flash('loginMessage');
            req.url == "/"
                ? Transporter.render(res,'index',m.GETTED)
                : next();
        },

        contato: function contato(req, res, next) {
            var m = new Messages();
            m.GETTED.DATA = {
                titulo: 'Expirou App | Novo Contato.',
                mensagem : null
            };
            req.url == "/contato"
                ? Transporter.render(res,'contato',m.GETTED)
                : next();
        },

        cadastrar: function cadastrar(req, res, next) {
            var m = new Messages();
            m.GETTED.DATA = {
                titulo: 'Expirou App | Novo Cadastro.'
            };
            req.url == "/cadastrar"
                ? Transporter.render(res,'cadastrar',m.GETTED)
                : next();
        },

        entrar: function entrar(req, res, next) {
            var m = new Messages();
            m.GETTED.DATA = {
                titulo: 'Expirou App | Entrar.'
            };
            req.url == "/entrar"
                ? Transporter.render(res,'entrar',m.GETTED)
                : next();
        },

        enviarContato :  function(req, res, next) {
            var m = new Messages();
            var dest = req.body;
            var mail = new Mail();

            var config = {
                name : 'GMAIL',
                sender : 'contato.expirou@gmail.com',
                auth : 'Security',
                headers : {
                    from: 'contato.expirou@gmail.com',
                    to: 'contato.expirou@gmail.com',
                    subject: 'Novo Contato | Expirou App - ' + dest.name,
                    html: '<p>' +dest.message+'</p><p>E-mail:'+ dest.email +'</p>'
                },
                error: function error() {
                    m.CREATEERROR.DATA = {
                        titulo: 'Expirou App | Novo Contato - Não foi possível enviar sua mensagem!',
                        mensagem : 'Não foi possível enviar sua mensagem!'
                    };
                    Transporter.render(res,'contato',m.CREATEERROR);
                },
                success : function success() {

                    m.CREATED.DATA = {
                        titulo: 'Expirou App | Novo Contato - Mensagem enviada com sucesso!',
                        mensagem : 'Mensagem enviada com sucesso!'
                    };

                    Transporter.render(res,'contato',m.CREATED);
                }
            };

            mail.send(config);
        },

        manager : function manager(req, res, next){
            var m = new Messages();
            m.GETTED.DATA = {
                titulo : 'Arquitetura ADMIN',
                usuario : req.user
                    ? req.user
                    : null
            };
            req.url == "/manager"
                ? Transporter.render(res,'dashboard',m.GETTED)
                : next();
        }
    };
    return controller;
};
module.exports = new HomeController();



