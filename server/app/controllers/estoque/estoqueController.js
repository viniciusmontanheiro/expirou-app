'use strict';

var MovimentacaoProduto = require("../../models/MovimentacaoProduto");
var Enums = require("../../models/Enums");

var EstoqueController = {};

EstoqueController.listAll = function(req, res, next){
    var m = new Messages();

    MovimentacaoProduto.find(function (err, movimentacoes) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = movimentacoes;
        return Transporter.json(res, m.GETTED);
    });
};

EstoqueController.create = function(req, res, next){

};

EstoqueController.update = function(req, res, next){

};

EstoqueController.remove = function(req, res, next){

};

module.exports = EstoqueController;
