'use strict';

/**
 * @description Implementação do ProdutoController
 */

var Produto = require("../../models/Produto");
var CategoriaProduto = require("../../models/CategoriaProduto");
var Enums = require("../../models/Enums");

var ProdutoController = {};

//ListAction - Lista todos os produtos cadastrados
//========================================
ProdutoController.list = function(req, res, next){
	var m = new Messages();
	
    Produto.find(function (err, produtos) {
    	m.GETTED.DATA = produtos;
        return Transporter.json(res, m.GETTED);
    });
};

//CreateAction
//========================================
ProdutoController.create = function(req, res, next){
	var m = new Messages();
	var produto = new Produto(req.body);

	produto.save(function(err,data){
		if(err)
			return Transporter.json(res, m.CREATEERROR);
		return Transporter.json(res, m.CREATED);
	});
};


//UpdateAction
//========================================
ProdutoController.update = function(req, res, next){
	var m = new Messages();
    var produto = req.body;

    if (Util.isEmpty(produto,['_id']))
        return Transporter.emit(res, m.CHANGEERROR);
    
    
	Produto.update({ _id: produto._id }, { 
		$set: produto
	}, function(err, user){
		if (err) {
            console.error(err);
            return Transporter.json(res, m.CHANGEERROR);
        }
        return Transporter.json(res, m.CHANGED);
	});
	
};


//RemoveAction
//========================================
ProdutoController.remove = function(req, res, next){
	var m = new Messages();
    var id = req.params.id;

    Produto.find({ _id: id }).remove(function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.REMOVEERROR);
        }
        return Transporter.json(res, m.REMOVED);
    });
};

//Listar Categorias de Produtos
//========================================
ProdutoController.listCategorias = function(req, res, next){
	var m = new Messages();
	
	CategoriaProduto.find(function (err, categorias) {
    	
    	categorias = [{
    		id: 1,
    		descricao: "Doces"
    	},{
    		id: 2,
    		descricao: "Bolos"
    	},{
    		id: 3,
    		descricao: "Balas"
    	}];
    	
        m.GETTED.DATA = categorias;
        return Transporter.json(res, m.GETTED);
    });
};


module.exports = ProdutoController; 




