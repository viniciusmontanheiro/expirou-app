'use strict';

/**
 * @description Implementação do index controller
 */

var User = require("../../models/User");

var UserController = {};

//Create
//========================================
UserController.create = function(passport){
	return passport.authenticate('new-account', {
        successRedirect: '/manager',
        failureRedirect: '/'
    });
};


//ListAll
//========================================
UserController.listAll = function(req, res, next){
	var m = new Messages();
	
	User.find(function (err, users) {
	    if (err) {
	        console.error(err);
	        return Transporter.json(res, m.HANDLEERROR);
	    }
	
	    m.GETTED.DATA = users;
	    return Transporter.json(res, m.GETTED);
	});
};

//Update
//========================================
UserController.update = function(req, res, next){
	var m = new Messages();
    var user = req.body;

    if (Util.isEmpty(user,['_id'])) {
        return Transporter.emit(res, m.CHANGEERROR);
    }else{
        if(Util.isEmpty(user,'password')){
            delete user.password;
        }else{
            user.password = User.generateHash(user.password);
        }
    }

    var id = user._id;
    delete user._id;

    User.update({_id: id}, user, {upsert: true}, function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.CHANGEERROR);
        }
        return Transporter.json(res, m.CHANGED);
    });
};

//Delete
//========================================
UserController.delete = function(req, res, next){
	var m = new Messages();
    var id = req.params.id;

    User.find({ _id: id }).remove(function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.REMOVEERROR);
        }
        return Transporter.json(res, m.REMOVED);
    });
};


module.exports = UserController;



