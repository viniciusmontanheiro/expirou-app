'use strict';

/**
 * @description Unificação das rotas do sistema.
 * @param app
 */

var homeController = require('./controllers/home/home');
var loginController = require('./controllers/login/login');
var genericController = require('./controllers/generic/generic');
var imagesController = require('./controllers/images/images');
var userController = require('./controllers/user/user');

function Routes(passport) {

    //INDEX ROUTES
    app.get('/', homeController.home);
    app.get('/contato', homeController.contato);
    app.get('/cadastrar', homeController.cadastrar);
    app.get('/entrar', homeController.entrar);
    app.post('/login',loginController.login(passport));
    app.post('/login',loginController.login(passport));
    app.post('/contato', homeController.enviarContato);
    app.get('/logout', isLoggedIn,loginController.logout);

    //ADMIN ROUTES
    app.get('/manager',isLoggedIn, homeController.manager);

    // Grupo de Acesso - Users
    //================================================
    var userController = require('./controllers/user/userController');

    app.post('/user/create',userController.create(passport));
    app.get('/user/list', userController.listAll);
    app.delete('/user/delete/:id', userController.delete);
    app.put('/user/update', userController.update);

    // Produtos
    //================================================
    var produtoController = require('./controllers/produto/produtoController');
    app.get('/product/list', isLoggedIn, produtoController.list);
    app.post('/product/create', isLoggedIn, produtoController.create);
    app.put('/product/update', isLoggedIn, produtoController.update);
    app.delete('/product/remove/:id', isLoggedIn, produtoController.remove);
    app.get('/product/categorias/list', isLoggedIn, produtoController.listCategorias);
    
    // Estoque
    //================================================
    var estoqueController = require('./controllers/estoque/estoqueController');
    //app.get('/estoque', isLoggedIn, estoqueController.list);

    app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    app.get('/connect/local', function (req, res) {
        res.render('connect-local.ejs', { message: req.flash('loginMessage') });
    });
    app.post('/connect/local', passport.authenticate('local-signup', {
        successRedirect: '/profile',
        failureRedirect: '/connect/local',
        failureFlash: true
    }));

    //app.get('/connect/facebook', passport.authorize('facebook', { scope: 'email' }));
    //
    //app.get('/connect/facebook/callback',
    //    passport.authorize('facebook', {
    //        successRedirect: '/profile',
    //        failureRedirect: '/'
    //    }));

    app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));

    app.get('/unlink/local', function (req, res) {
        var user = req.user;
        user.local.email = undefined;
        user.local.password = undefined;
        user.save(function (err) {
            res.redirect('/profile');
        });
    });

    app.get('/unlink/facebook', function (req, res) {
        var user = req.user;
        user.facebook.token = undefined;
        user.save(function (err) {
            res.redirect('/profile');
        });
    });

    
//    app.get('/product/list', isLoggedIn, produtoController.list);
//    app.post('/product/remove/item', isLoggedIn, produtoController.delete);
//    app.put('/product/update/:id', isLoggedIn, produtoController.update);
    
    //// Imagens
    //var imagesController = require('./controllers/imagesController');
    app.post('/images', imagesController.create);
    app.post('/upload', imagesController.upload);

    // Produtos
    //var productController = require('./controllers/productController');
    //app.post('/product/item/new', isLoggedIn, productController.create);
    //app.get('/product/list', isLoggedIn, productController.list);
    //app.post('/product/remove/item', isLoggedIn, productController.delete);
    //app.put('/product/update/:id', isLoggedIn, productController.update);
    //
    //// Imagens
    //var imagesController = require('./controllers/imagesController');
    //app.post('/images', imagesController.create);
    //app.post('/upload', imagesController.upload);
    //app.get('/images/:produto_id', imagesController.listaByProdutoId);
    //app.get('/images/list/:_id:produto_id', imagesController.listaByImageIdProdutoId);
    //app.put('/image/update/:item', imagesController.update);
    //app.delete('/image/remove/:id', imagesController.delete);


    //ISSUE ROUTES
    //================================================
    app.all("*", genericController.error.notFound);
};

// Verifica se o usuário está autenticado
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()){
        return next();
    }
    res.redirect('/');
}

//Disponibilizando na aplicação
GLOBAL.isLoggedIn = isLoggedIn;

module.exports = Routes;





