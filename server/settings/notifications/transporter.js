'use strict';

/**
 * @description Processa ás requisições com logs de informações
 */
function Transporter() {

    /**
     * @description Disponibilizando o status para o programador
     * @type {{}}
     */
    this.status = {};

    /**
     * @desc Processa log de mensagens
     * @param response
     * @param err
     * @returns {*|ServerResponse}
     */
    this.json = function (response, status, beforeSend) {
        if(beforeSend != undefined && beforeSend != null){
            beforeSend();
        }
        response.status(status.CODE).json(this.getLogMessage(status));
    };

    /**
     * @desc Processa log de mensagens
     * @param response
     * @param err
     * @returns {*|ServerResponse}
     */
    this.render = function (response,view,status,beforeSend) {
        if(beforeSend != undefined && beforeSend != null){
            beforeSend();
        }
        response.status(status.CODE).render(view, this.getLogMessage(status));
    };

    /**
     * @desc Recupera mensagem informada
     * @param info
     * @returns {* mensagem concatenada }
     */
    this.getLogMessage = function (status) {
        status.TYPE === "error"
            ? console.error(status.CODE + " - " + status.MESSAGE + " [" + status.ADDITIONAL + "] \n")
            : console.info(status.CODE + " - " + status.MESSAGE + " [" + status.ADDITIONAL + "] DATA:" + JSON.stringify(status.DATA) + "\n");
        return status;
    };
};

module.exports =  Transporter;
