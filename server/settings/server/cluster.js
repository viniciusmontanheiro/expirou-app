'use strict';

/**
 * Created by vinicius on 06/04/15.
 */

var cluster = require("cluster");
var Server = require("./server.js");
var util = require("util");
var os = require("os");

function Clustering() {

    this.numCPUs = os.cpus().length;

    this.init = function () {
        //Verificando se o processo pai é único
        if (cluster.isMaster) {
            console.info('Aplicação iniciada com %d clusters', this.numCPUs);

            //Criando os filhos baseando no número de CPUS
            for (var i = 0; i < this.numCPUs; i++) {
                cluster.fork();
            }

            cluster.on('online', function (workers) {
                console.info("Instância[" + workers.id + "]" + ": PID[" + workers.process.pid + "] - " + workers.state);
            });

            cluster.on('exit', function (worker, code, signal) {
                //Se fizermos kill em algum dos filhos
                if (worker.suicide === true) {
                    console.warn('Oh, isso foi suicídio!\' – não se preocupe.');
                    //Se não
                } else {
                    console.info('Processo %d morto (%s)', worker);
                }
            });

            cluster.on('message', function (msg) {
                console.info("Mensagens para os filhos: (%s)" + msg);
                if (msg == "exit") {
                    this.killAll();
                }
            });

        } else {
            //Compartilhando o servidor entre os filhos
            Server();
        }
    };

    this.killAll = function () {
        for (var id in cluster.workers) {
            cluster.workers[id].kill(id);
        }
        process.exit(0);
    };

    this.getMemoryInfo = function () {
        console.info(util.inspect(process.memoryUsage()));
    }
};
module.exports = Clustering;





