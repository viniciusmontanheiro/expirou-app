var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var User = require('../../app/models/User');
var auth = require('./auth');

module.exports = function (passport) {

    var messages = new Messages();

    // Serializa o usuário na sessão
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // Remove o usuário da sessão
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    passport.use('new-account', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {

            process.nextTick(function () {
                User.findOne({'email': email})
                    .populate('user', '-password')
                    .exec(function (err, user) {

                        var response = req.res;

                        if (err) {
                            console.error(err);
                            return Transporter.json(req.res, messages.ACCOUNTERROR)
                        }

                        // Verifica se já existe usuário logado com esse email
                        if (user) {
                            return Transporter.json(response, messages.USEREXISTS);
                        } else {

                            //Se não, o criamos
                            var newUser = new User();
                            newUser.nome = req.body.nome;
                            newUser.email = req.body.email;
                            newUser.password = newUser.generateHash(password);
                            newUser.ativo = req.body.ativo;
                            newUser.telefone = req.body.telefone;

                            // Salva o usuário
                            newUser.save(function (err) {
                                if (err) {
                                    console.error(err);
                                    return Transporter.json(response, messages.CREATEERROR);
                                }
                                Transporter.json(response, messages.USERCREATED);
                            });
                        }
                    });


            });

        }));

    passport.use('admin-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {

            User.findOne({'email': email})
                .populate('user', '-password')
                .exec(function (err, user) {
                    if (err) {
                        console.error(err);
                        return done(err);
                    }

                    // Se o usuário não for encontrado ou se o password for inválido
                    if (!user || !user.validPassword(password)) {
                        console.error(err);
                        return done(null, false, req.flash('loginMessage', messages.LOGINERROR.MESSAGE));
                    }
                    console.info("Usuário " + email + " logado!");
                    done(null, user);
                })
        }));

    passport.use(new FacebookStrategy({

            clientID        : auth.facebookAuth.clientID,
            clientSecret    : auth.facebookAuth.clientSecret,
            callbackURL     : auth.facebookAuth.callbackURL

        },

        // facebook will send back the token and profile
        function(token, refreshToken, profile, done) {

            // asynchronous
            process.nextTick(function() {

                // find the user in the database based on their facebook id
                User.findOne({ 'facebook.id' : profile.id }, function(err, user) {


                    if (err){
                        return done(err);
                    }

                    // if the user is found, then log them in
                    if (user) {
                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user found with that facebook id, create them
                        var newUser = new User();

                        // set all of the facebook information in our user model
                        newUser.facebook.id    = profile.id; // set the users facebook id
                        newUser.facebook.token = token; // we will save the token that facebook provides to the user
                        newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                        newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first

                        // save our user to the database
                        newUser.save(function(err) {
                            if (err)
                                throw err;

                            // if successful, return the new user
                            return done(null, newUser);
                        });
                    }

                });
            });

        }));

};