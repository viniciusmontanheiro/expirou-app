'use strict';

angular.module('app.estoque', []).controller('EstoqueController', ['ApiService', 'MessageService', 'DialogService', 'ComponentsServices', EstoqueController]);

function EstoqueController(ApiService, MessageService, DialogService, ComponentsServices) {

    var self = this;

    self.movimentacoes = [];
    carregarMovimentacoes();

    self.showDialogNewMovimentacao = function() {
        var dialog = angular.copy(DialogService);
        dialog
            .confirm(event, "components/estoque/cadastro.html", new MovimentacaoProduto())
            .then(function (movimentacaoProduto) {
                self.criarMovimentacao(movimentacaoProduto);
            }, function () {
                Manager.clear();
            });
    };

    self.criarMovimentacao = function (movimentacao) {

    };

    self.excluirMovimentacao = function (movimentacao) {

    };

    self.alterarMovimentacao = function (movimentacao) {

    };

    function carregarMovimentacoes() {
        var success = function (produtos) {
            self.produtos = angular.copy(produtos.DATA);
        };

        var error = function (err) {
            console.log(err);
        };

        ApiService.query("/product/list").success(success).error(error);
    }

    function MovimentacaoProduto() {
        return {
            tipoMovimentacao   : 1,
            produto     : {
                id: 'asdfasdfaadfasdf',
                nome : 'Teste',
                descricao : 'Testando produto Teste',
                unidadeMedida : 0
            },
            observacao     : "Saldo iniciala sdfbaksdhfagndsiua hfasdyfaykasfkvadskybfadsufladbf ,abgaa fghal",
            quantidade : 1230,
            dataMovimentacao: new Date()
        };
    }

    self.getDescricaoUnidadeMedida = function (unidadeMedida) {
        return Enums.UnidadeMedida.findUnidadeMedida(unidadeMedida).descricao;
    };

    self.getIconTipoMovimentacao = function(tipoMovimentacao) {
        return Enums.TipoMovimentacao.findTipoMovimentacao(tipoMovimentacao).icone;
    };

    self.getColorTipoMovimentacao = function(tipoMovimentacao) {
        return Enums.TipoMovimentacao.findTipoMovimentacao(tipoMovimentacao).cor;
    };

}