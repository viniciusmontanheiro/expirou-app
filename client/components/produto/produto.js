'use strict';

<<<<<<< HEAD

angular.module('app.produto', ['generalDirectives','formDirectives']).controller('ProdutoController', ['ApiService', 'MessageService', 'DialogService', 'ComponentsServices', '$http', '$mdDialog', ProdutoController]);

function ProdutoController(ApiService, MessageService, DialogService, ComponentsServices, $http, $mdDialog) {
    var self = this;
	this.produtos = [];
	this.categorias = [];
	this.unidadesMedida = angular.copy(Enums.UnidadeMedida.values);
	this.showDialogNewProduct = showDialogNewProduct;
    this.showDialogEditItem = showDialogEditItem;
    this.confirmarRemoverItem = confirmarRemoverItem;
    this.showDialogCategorias = showDialogCategorias;
    
    
    //Init
    //================================================
    listarProdutos();	
    getCategorias();
        
    
    //GetCategorias
    //================================================
    function getCategorias(){
    	var success = function(categorias){
    		self.categorias = angular.copy(categorias.data.DATA);
    	};
    	var err = function(e){
    		console.log("Erro ao obter categorias", e);
    	};
    	//$http.get("/product/categorias/list").then(success, error);
    	//ApiService.query("/product/categorias/list").then(success, error);
    	self.categorias = [{
    		id: 1,
    		descricao: "Doces"
    	},{
    		id: 2,
    		descricao: "Bolos"
    	},{
    		id: 3,
    		descricao: "Balas"
    	}];
    }
    
    //Listar produtos
    //================================================
    function listarProdutos(){
    	var success = function (produtos) {
            self.produtos = angular.copy(produtos.data.DATA);
        };

        var error = function (err) {
            console.log(err);
        };
        $http.get("/product/list").then(success, error);
        //new ApiService().query("/product/list").then(success, error);
    }
    
    //Mostrar Dialog para cadastro de produtos
    //================================================
    function showDialogNewProduct(){
    	var dialog = angular.copy(DialogService);
    	var p = new Model.Produto();
    	p.categorias = angular.copy(self.categorias);
    	p.unidadesMedida = angular.copy(self.unidadesMedida);
        dialog
        	.confirm(event, "components/produto/cadastro.html", p)
            .then(function (produto) {
            	delete produto.categorias;
            	delete produto.unidadesMedida;
            	criarProduto(produto);
            }, function () {
               // Manager.clear();
            });
    }
    
    
    //Cria o produto
    //================================================
    function criarProduto(produto){
    	
    	//TODO: ajustar na diretiva para ja pegar o valor formatado
    	produto.preco = produto.preco.replace("R$ ","").replace(",",".");
    	
    	var success = function (data) {
            listarProdutos();
            MessageService.addSuccess(data.MESSAGE);
        };

        var error = function (err) {
            MessageService.addError(err.MESSAGE);
        };

        ApiService.save("/product/create", produto).success(success).error(error);
    }
    
    //showDialogEditItem
    //================================================
    function showDialogEditItem(produto, event){
    	var dialog = angular.copy(DialogService);
    	produto.isEditMode = true;
    	produto.situacaoProduto = (produto.situacaoProduto == 1); //1 = ativo
    	produto.categorias = angular.copy(self.categorias);
    	produto.unidadesMedida = angular.copy(self.unidadesMedida);
    	
        dialog
        	.confirm(event, "components/produto/cadastro.html", produto)
            .then(function (produto) {
            	alterarProduto(produto);
            }, function () {
                //Manager.clear();
            });
    }

    //AlterarProduto
    //================================================    
    function alterarProduto(produto){
    	delete produto.categorias;
    	delete produto.unidadesMedida;
    	
    	var success = function (data) {
    		MessageService.addSuccess(data.data.MESSAGE);
    		listarProdutos();
        };

        var error = function (err) {
            MessageService.addError(err.data.MESSAGE);
        };

        ApiService.update("/product/update", produto).then(success, error);
    }
    
    
    //Confirmar remover item
    //================================================
    function confirmarRemoverItem(produto, event){
    	
		var confirm = $mdDialog
			.confirm()
			.title('Deseja remover ' + produto.nome + "?")
			.content('Este produto será removido e não poderá ser visualizado pelos clientes.')
			.ariaLabel('Remover Item')
			.targetEvent(event)
			.ok('Confirmar')
			.cancel('Cancelar');
		
		$mdDialog
			.show(confirm)
			.then(function() {
				removerProduto(produto)//remover
			});
    	
    }
    
    //RemoverProduto
    //================================================ 
    function removerProduto(produto){
    	ApiService.remove("/product/remove/", produto._id).then(function(data){
    		MessageService.addSuccess(data.data.MESSAGE);
    		listarProdutos();
    	}, function(err){
    		MessageService.addError(err.data.MESSAGE);
    	});
    }
    
    //Opcoes das categorias
    //================================================ 
    function showDialogCategorias(event){
    	var dialog = angular.copy(DialogService);
    	var model = {
			categorias : angular.copy(self.categorias)
    	};
    	dialog
	    	.confirm(event, "components/produto/categorias.html", model)
	        .then(function () {
	        	
	        }, function () {
	            //Manager.clear();
	        });
    }
    
    
}


=======
/**
 * Created by viniciusmontanheiro on 23/06/15.
 */

angular.module('app.produto', [])
    .controller('ProdutoController', [function () {
        this.titulo = 'ProdutoController';

    }]);
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308
