'use strict';
<<<<<<< HEAD

=======
/**
 * Created by viniciusmontanheiro on 15/07/15.
 */
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308

angular.module('app.usuario', ['generalDirectives'])
    .controller('UsuarioController', ['ApiService', 'MessageService', 'DialogService', 'ComponentsServices',
        function (ApiService, MessageService, DialogService, ComponentsServices) {

            var self = this;
            var model = {
                ativo: false,
                nome: "",
                email: "",
                telefone: "",
                password: "",
                ultimaAtualizacao: new Date(),
                collection: [],
                url: {
                    list: '/user/list',
                    registerTemplate: 'components/usuario/cadastro.html',
                    removeConfirm:'components/usuario/removeConfirm.html',
                    remove: '/user/delete/',
                    create: '/user/create',
                    update: '/user/update'
                },
                isDeleteMode: false,
                isEditMode: false
            };

            self.model = angular.copy(model);

            var Manager = new ComponentsServices.SelectionManager("user-container md-list-item", "btn-addUser");

            (function () {
                //Busca todos os usuários
                listUsers();
            })();

            function listUsers() {

                var success = function (usuarios) {
                    self.model.collection = usuarios.DATA;
                    clearForm();
                };

                var error = function (err) {
                    console.log(err);
                };

                ApiService.query(self.model.url.list).success(success).error(error);
            };

            self.selectItem = function (model, event) {
                Manager.select(model, event);
                setCurrentMode(Manager.getIsDeleteMode(), Manager.getIsEditMode());
            };

            self.editItem = function (model, event) {
                Manager.edit(model, event);

                self.model = angular.extend(self.model, model);
                var dialog = angular.copy(DialogService);
                self.model.password = "";

                dialog.confirm(event, self.model.url.registerTemplate, self.model)
                    .then(function (usuario) {
                        usuario.ultimaAtualizacao = new Date();
                        //Atualiza o usuário
                        self.update(usuario);
                    }, function () {
                        Manager.clear();
                    });
            };

            /**
             * Exibe o formulário de cadastro
             * @param event
             */
            self.showAdd = function (event) {
                clearForm();

                var dialog = angular.copy(DialogService);
                dialog.confirm(event, self.model.url.registerTemplate, self.model)
                    .then(function (usuario) {
                        //Cria o novo usuário
                        self.create(usuario);
                    }, function () {
                        Manager.clear();
                    });
            };

            /**
             * Exibe o formulário de cadastro
             * @param event
             */
            self.showRemove = function (event) {
                var dialog = angular.copy(DialogService);
                dialog.confirm(event, self.model.url.removeConfirm, self.model)
                    .then(function (usuario) {
                        //Remove o usuário
                        self.remove();
                    }, function () {
                        Manager.clear();
                        clearForm();
                    });
            };

            /**
             * Cria o novo usuário
             * @param usuario
             */
            self.create = function (usuario) {
                var success = function (data) {
                    listUsers();
                    MessageService.addSuccess(data.MESSAGE);
                };

                var error = function (err) {
                    MessageService.addError(err.MESSAGE);
                };

                ApiService.save(self.model.url.create, usuario).success(success).error(error);
            };

            self.update = function update(usuario) {
                var success = function (data) {
                    Manager.clear();
                    listUsers();
                    setCurrentMode(Manager.getIsDeleteMode(), Manager.getIsEditMode());
                    MessageService.addSuccess(data.MESSAGE);
                };
                var error = function (err) {
                    Manager.clear();
                    setCurrentMode(Manager.getIsDeleteMode(), Manager.getIsEditMode());
                    MessageService.addError(err.MESSAGE);
                };

                if (usuario) {

                    var update = {};
                    update._id = usuario._id;
                    update.nome = usuario.nome;
                    update.email = usuario.email;
                    update.ultimaAtualizacao = new Date();
                    update.password = usuario.password;
                    update.ativo = usuario.ativo;
                    update.telefone = usuario.telefone;

                    ApiService.update(self.model.url.update, update).success(success).error(error);
                }
            };

            self.remove = function () {
                var success = function (data) {
                    Manager.clear();
                    listUsers();
                    setCurrentMode(Manager.getIsDeleteMode(), Manager.getIsEditMode());
                    MessageService.addSuccess(data.MESSAGE);
                    self.model.password = "";
                };
                var error = function (err) {
                    Manager.clear();
                    setCurrentMode(Manager.getIsDeleteMode(), Manager.getIsEditMode());
                    MessageService.addError(data.MESSAGE);
                };

                if (Manager.getIsDeleteMode()) {
                    var list = Manager.getSelectedList();

                    if (list.length >= 1) {
                        list.forEach(function (user) {
                            //Remove usuários
                            ApiService.remove(self.model.url.remove, user._id).success(success).error(error);
                        })
                    }
                }
            };

            function setCurrentMode(modeDelete, modeEdit) {
                self.model.isDeleteMode = modeDelete;
                self.model.isEditMode = modeEdit;
            };

            function clearForm(){
              var arr = self.model.collection;
              self.model = angular.copy(model);
              self.model.collection = arr;
            };

        }]);