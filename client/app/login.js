'use strict';

(function() {

    /**
     * Dependências
     */
    angular.module('admin-login',
        ['ngNewRouter'
            ,'ngAnimate'
            ,'ngAria'
            ,'ngMaterial'
            ,'toaster'
            ,'ngMessages'
            ,'appServices'
        ])
        .controller('LoginController', LoginController)
        .config(function($mdThemingProvider,$locationProvider) {
            var customBlueMap = 		$mdThemingProvider.extendPalette('light-blue', {
                'contrastDefaultColor': 'light',
                'contrastDarkColors': ['50'],
                '50': 'ffffff'
            });
            $mdThemingProvider.definePalette('customBlue', customBlueMap);
            $mdThemingProvider.theme('default')
                .primaryPalette('customBlue', {
                    'default': '500',
                    'hue-1': '50'
                })
                .accentPalette('pink');
            $mdThemingProvider.theme('input', 'default')
                .primaryPalette('grey');

        });

    LoginController.$inject = [
        'MessageService'
    ];

    //Main controller
    function LoginController(MessageService) {
        this.showError = true;
        this.error = function (){
            this.showError = false;
            return MessageService.addError("Usuário ou senha inválidos!");
        };
    };

})();


