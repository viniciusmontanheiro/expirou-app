'use strict';

/**
 * Aplicação MEAN Stack Real Time voltada para comércio virtual
 * @name WhyNot
 * @requires NodeJs, AngularJs, MongoDB, Bower, GruntJs
 * @since 21/06/2015
 * @author Vinícius Montanheiro
 * @copyright WhyNot
 * @version 1.0.0
 * @licence MIT
 */

(function() {

    /**
     * Dependências
     */
    angular.module('arquitetura-admin',
        ['ngNewRouter'
            ,'ngAnimate'
            ,'ngAria'
            ,'ngMaterial'
            ,'ngMessages'
            ,'toaster'
            ,'ngMdIcons'
<<<<<<< HEAD
            ,'angularFileUpload'
=======
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308
            ,'appServices'
            ,'app.home'
            ,'app.produto'
            ,'app.usuario'
<<<<<<< HEAD
            ,'app.exemplo'
            //,'app.estoque'

=======
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308
        ])
        .controller('AppController', AppController)
        .config(function($mdThemingProvider) {
            var customBlueMap = 		$mdThemingProvider.extendPalette('light-blue', {
                'contrastDefaultColor': 'light',
                'contrastDarkColors': ['50'],
                '50': 'ffffff'
            });
            $mdThemingProvider.definePalette('customBlue', customBlueMap);
            $mdThemingProvider.theme('default')
                .primaryPalette('customBlue', {
                    'default': '500',
                    'hue-1': '50',
                    'hue-2':'700'
                })
                .accentPalette('orange', {
                    'default': '500'
                })
                .warnPalette('red',{
                    'default': '500'
                });
            $mdThemingProvider.theme('input', 'default')
                .primaryPalette('grey');
        });

    AppController.$inject = [
        '$scope'
        ,'$mdSidenav'
    ];

    //Rotas
    AppController.$routeConfig = [
        { path: '/', component: 'home' },
        { path : '/produtos', component: 'produto' },
        { path : '/usuarios', component: 'usuario' },
<<<<<<< HEAD
        { path : '/estoque', component: 'estoque' },
        { path : '/exemplo', component: 'exemplo' }
=======
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308
    ];

    //Main controller
    function AppController($scope, $mdSidenav) {

        //Mostra o menu para versão responsiva
        this.toggleSidenav = function(menuId) {
            $mdSidenav(menuId).toggle();
        };

        //Fecha o menu
        this.close = function () {
            $mdSidenav('left').close();
        };

        this.menu = [
            {
                link : 'home',
                title: 'Dashboard',
                icon: 'dashboard'
            },
            {
                link : 'produto',
                title: 'Produtos',
                icon: 'view_list'
            },
            {
<<<<<<< HEAD
                link : 'estoque',
                title: 'Estoque',
                icon: 'import_export'
            },
            {
=======
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308
                link : 'produto',
                title: 'Vendas',
                icon: 'credit_card'
            },
            {
<<<<<<< HEAD
                link : 'exemplo',
                title: 'exemplo',
                icon: 'group'
=======
                link : 'produto',
                title: 'Mensagens',
                icon: 'message'
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308
            }
        ];

        this.settings = [
            {
                link : 'usuario',
                title: 'Grupo de Acesso',
                icon: 'group'
            },
            {
                link : 'produto',
                title: 'Lixeira',
                icon: 'delete'
            },
            {
                link : 'admin.showListBottomSheet($event)',
                title: 'Configurações',
                icon: 'settings'
            }
        ];
<<<<<<< HEAD
    }
=======
    };
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308

})();


