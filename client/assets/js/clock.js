/**
 * Created by vcrzy on 05/01/16.
 * @source http://codepen.io/HughDai/pen/MKKXJp
 * @author Hugh Dai
 */

(function(){

    $.material.init();
    $.material.ripples();
    $.material.input();
    $.material.checkbox();
    $.material.radio();

    //generate clock animations
    var animations = document.getElementById("clock-animations");

    if(animations){
        var now       = new Date(),
            hourDeg   = now.getHours() / 12 * 360 + now.getMinutes() / 60 * 30,
            minuteDeg = now.getMinutes() / 60 * 360 + now.getSeconds() / 60 * 6,
            secondDeg = now.getSeconds() / 60 * 360,
            stylesDeg = [
                "@-webkit-keyframes rotate-hour{from{transform:rotate(" + hourDeg + "deg);}to{transform:rotate(" + (hourDeg + 360) + "deg);}}",
                "@-webkit-keyframes rotate-minute{from{transform:rotate(" + minuteDeg + "deg);}to{transform:rotate(" + (minuteDeg + 360) + "deg);}}",
                "@-webkit-keyframes rotate-second{from{transform:rotate(" + secondDeg + "deg);}to{transform:rotate(" + (secondDeg + 360) + "deg);}}",
                "@-moz-keyframes rotate-hour{from{transform:rotate(" + hourDeg + "deg);}to{transform:rotate(" + (hourDeg + 360) + "deg);}}",
                "@-moz-keyframes rotate-minute{from{transform:rotate(" + minuteDeg + "deg);}to{transform:rotate(" + (minuteDeg + 360) + "deg);}}",
                "@-moz-keyframes rotate-second{from{transform:rotate(" + secondDeg + "deg);}to{transform:rotate(" + (secondDeg + 360) + "deg);}}"
            ].join("");

        animations.innerHTML = stylesDeg;
    }

    var signupForm = $("#signup-form");
    var signinForm = $("#signin-form");
    var contactForm = $("#contact-form");

    if(signupForm){
        signupForm.validate({
            rules: {
                name : "required",
                email: {
                    required: true,
                    email: true
                },
                password : {
                    required : true,
                    minlength: 6,
                    maxlength : 8
                },
                passwordConfirm : {
                    required : true,
                    minlength: 6,
                    maxlength : 8,
                    equalTo : "#password"
                }
            },
            messages: {
                name: "Por favor, informe o nome",
                email: "Este endereço de e-mail não é válido",
                password: {
                    required: "Por favor, informe a senha",
                    minlength: "A senha deve conter no mínimo 6 caracteres",
                    maxlenght: "A senha deve conter no máximo 8 caracteres"
                },
                passwordConfirm : {
                    required: "Por favor, confirme seu password",
                    minlength: "A senha deve conter no mínimo 6 caracteres",
                    maxlenght: "A senha deve conter no máximo 8 caracteres",
                    equalTo : "Confirmação de senha inválida"
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });
    }

    if(signinForm) {
        signinForm.validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 8
                }
            },
            messages: {
                email: "Este endereço de e-mail não é válido",
                password: {
                    required: "Por favor, informe a senha",
                    minlength: "A senha deve conter no mínimo 6 caracteres",
                    maxlenght: "A senha deve conter no máximo 8 caracteres"
                }
            },

            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    if(contactForm){
        contactForm.validate({
            rules: {
                name : "required",
                email: {
                    required: true,
                    email: true
                },
                message : "required"
            },
            messages: {
                name: "Por favor, informe o nome",
                email: "Este endereço de e-mail não é válido",
                message: "Por favor, informe o conteúdo da mensagem"
            },

            submitHandler: function(form) {
                form.submit();
            }
        });
    }

    $('.fa-close').css({'cursor':'pointer'}).click(function(){
        $('.message-success').remove();
    });

})();

